requests==2.25.1
environs==9.3.0
pandas==1.5.3
openpyxl==3.1.1
tqdm==4.65.0
plotly