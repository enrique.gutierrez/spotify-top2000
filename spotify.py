import base64
import requests
from environs import Env
import time

# Load environemnt variable secrets file (`.env`)
env = Env()
env.read_env()


class SpotifyAPI:
    """
    Interface for connecting with the Spotify WebAPI
    """

    def __init__(self):
        self.client_id = env("CLIENT_ID")
        self.client_secret = env("CLIENT_SECRET")
        self.token = self._get_token()

    def _get_token(self):
        """ Based on Client Credential Flow from: 
        https://developer.spotify.com/documentation/general/guides/authorization-guide/#client-credentials-flow
        """
        # Get Base64 of CLIENT-ID
        b64_client = base64.b64encode((self.client_id + ':' + self.client_secret).encode('ascii')).decode('ascii')

        # Make HTTP request
        url = 'https://accounts.spotify.com/api/token'
        headers = {'Authorization': f'Basic {b64_client}'}
        data = {'grant_type': 'client_credentials'}
        response = requests.post(url=url, headers=headers, data=data)

        if response.status_code == 200 or response.status_code == 201:
            # Return the token string. Valid for 1hr
            return response.json()['access_token']
        else:
            raise ConnectionRefusedError("Unable to retrieve token")

    def get_track_id(self, query: str, sleep=0):
        time.sleep(sleep)

        url = 'https://api.spotify.com/v1/search'
        headers = {'Authorization': f'Bearer {self.token}'}
        try:
            response = requests.get(url=f"{url}?q={query.replace(' ', '+')}&type=track",
                                    headers=headers)
        except ConnectionError:
            return None

        if response.status_code == 200 or response.status_code == 201:
            # Return the song id for the best match
            items = response.json()['tracks']['items']
            if len(items) > 1:
                return items[0]['id']
            else:
                return None
        else:
            print(f"Unable to retrieve songs for {query}: {response.text}")
            return None

    def get_track_features(self, track_id: str, sleep=0):
        time.sleep(sleep)
        if not track_id:
            return {}

        url = f'https://api.spotify.com/v1/audio-features/{track_id}'
        headers = {'Authorization': f'Bearer {self.token}'}
        try:
            response = requests.get(url=url,
                                    headers=headers)
        except ConnectionError:
            return {}

        if response.status_code == 200 or response.status_code == 201:
            desired_features = [
                'id',
                'danceability',
                'energy',
                'key',
                'loudness',
                'mode',
                'speechiness',
                'acousticness',
                'instrumentalness',
                'liveness',
                'valence',
                'tempo'
                'duration_ms',
                'time_signature',
            ]
            # Return the song id for the best match
            out = {k: v for k, v in response.json().items() if k in desired_features}
            return out
        else:
            print(f"Unable to retrieve songs for {track_id}: {response.text}")
            return {}


if __name__ == "__main__":
    s = SpotifyAPI()
    track_id = s.get_track_id('Sound of Silence Disturbed')
    print(s.get_track_features(track_id))
