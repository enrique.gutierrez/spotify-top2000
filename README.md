# Spotify Top2000

Scrape data from the Spotify WebAPI to enrich NPO Radio's Top 2000 history

Data can be found [here](https://www.nporadio2.nl/top2000)